Write-Host (Get-Date).ToString()  "Running script" $PSCommandPath "..."

# All the packages that you want to be installed automatically; see:
# https://chocolatey.org/packages
$pkgs =
"chocolatey",
"chocolatey-core.extension",
"powershell-core",
"vscode",
"7zip",
"whatsapp",
"slack",
"microsoft-teams",
"jetbrainstoolbox",
"git",  # git client
"hg",   # mercurial client
"svn",  # Subversion client
"putty",
"nodejs",
"openssh",
"openssl.light",
"curl",
"wget",
"make",
"fiddler",
"python2",
"python3",
"dotnetcore-sdk",
"sql-server-management-studio",
"awscli",
"azure-cli",
"docker-for-windows",
"insomnia-rest-api-client",
"nssm",
"citrix-workspace"

# Some wont work ... unfortunately
$pkgs_manually =
"googlechrome",
"dropbox"
        