Write-Host (Get-Date).ToString()  "Running script" $PSCommandPath "..."

function SetEnv($key, $value) {
    [System.Environment]::SetEnvironmentVariable($key, $value, 'Machine')
}

function DeleteEnv($key) {
    # removing a env var from registry
    [System.Environment]::SetEnvironmentVariable($key, $null, "Machine")
}

function SetEnvUser($key, $value) {
    [Environment]::SetEnvironmentVariable($key, $value, 'User')
    # What also works is:
    # $env:HOME = $env:userprofile
    # But the long notation is used for consistency. 
}

function DeleteEnvUser($key) {
    # removing a env var from registry
    [Environment]::SetEnvironmentVariable($key, $null, "User")
}

# See:
# http://xahlee.info/powershell/environment_variables.html