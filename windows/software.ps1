Write-Host (Get-Date).ToString()  "Running script" $PSCommandPath "..."

# Imported scripts
. ./choco.packages.ps1
. ./utils.ps1
. ./environment.ps1

function Install()
{
    _check
    foreach ($p in $pkgs)
    {
        choco install -y $p
    }
    # Telling Git (and other Bash-like tools) to take the Windows-Home directory as its $HOME
    SetEnvUser HOME $env:USERPROFILE
    Write-Host " "
    Write-Host -ForegroundColor Green "Beware! Your %HOME% environment variable has been set to your Windows %USERPROFILE% directory. So that Git (and other Bash-like tools) have their homedir set to your Windows home-location."
    
    Write-Host " "
    Write-Host "Done! The following packages were not installed automagically. Please install them by hand."
    $pkgs_manually
}

function Upgrade()
{
    _check
    #    foreach ($p in $pkgs)
    #    {
    #        choco upgrade -y $p
    #    }
    choco upgrade all -y
    Write-Host " "
    Write-Host "Done! The following packages were not upgraded automagically. Please upgrade them by hand."
    $pkgs_manually
}

function Uninstall()
{
    _check
    foreach ($p in $pkgs)
    {
        choco uninstall -y $p
    }
    Write-Host " "
    Write-Host "Done! The following packages were not un-installed automagically. Please un-install them by hand."
    $pkgs_manually
}

function _check() {
    if (IsAdministrator)
    {
        # All OK       
    } else {
        throw "Software must be installed as an administrator, not as a regular user. Please call this function again from an elevated shell."
    }
}