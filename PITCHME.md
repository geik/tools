# Everything as Code

To view this as a presentation, go to:

<https://gitpitch.com/moviritpublic/tools?grs=gitlab>

---

## Everything? Yes we can!

+++

1. Provision Infrastructure & Shared services
1. Deploy apps onto Infrastructure
1. Build Software
1. Test Software
   1. Unit Test
   1. Integration Test
   1. Behavior Driven Development
   1. End-to-end Test
1. Define Business Logic in Software

---

## Provision Infrastructure & Shared services

+++

#### AWS Infra git repo

infra.yml
```yaml
- VPC (Virtual Private Cloud)
- Subnet(s)
- Internet Gateway, IGWAttachement, 
    RouteTable, Route, RouteTableAssociation
- Security Group (Restrict traffic to the VPC)
- ECS Cluster
- ECS Instance (=EC2 Instance), ECSInstanceProfile
- ECS Role (=IAM Role) 
    - ECS Policy
- ECS Task for Kafka (for defining Kafka docker-containers on ECS)
- ECS Service for Kafka (deploying Kafka docker-containers on ECS)
```
 
---

## Deploy apps onto Infrastructure

+++

#### App git repo  

db.yml
```yaml
- DynamoDB Table
```

repo.yml
```yaml
- ECS Docker Repository
```

app.yml
```yaml
- ECS Task (for defining the app docker-container on ECS)
- ECS Service (for deploying the app docker-container on ECS) 
```

---

## Build Software (CI/CD)

+++

gitlab-ci.yml
```yaml
stages:
  - prepare
  - build
  - integrate
  - deploy

prepare:
  stage: prepare
  script:
    - do something

build:
  stage: build
  script:
    - do something

itest:
  stage: integrate
  script:
    - do something

deploy:
  stage: deploy
  script:
    - do something
```

@[1-5](Define all the phases in the pipeline)
@[7-10](Define the commands/scripts per phase)
@[12-15](Define the commands/scripts per phase)
@[17-20](Define the commands/scripts per phase)
@[21-25](Define the commands/scripts per phase)

+++

![image](pipeline.png)

+++

gitlab-ci.yml (simplified example)
```yaml
stages:
  - prepare
  - build
  - integrate
  - deploy

prepare:
  stage: prepare
  script:
    - aws cloudformation deploy --stack-name mycompany-myapi-db
    - aws cloudformation deploy --stack-name mycompany-myapi-repo

build:
  stage: build
  script:
    - docker build dockerrepo.mycompany.nl/myapi:20180928 .
    - docker push dockerrepo.mycompany.nl/myapi:20180928

itest:
  stage: integrate
  script:
    - docker-compose --file ./Docker/docker-compose-itest.yml up -d
    - dotnet test MyApi.IntegrationTests

deploy:
  stage: deploy
  script:
    - aws cloudformation deploy --stack-name mycompany-myapi-app
```

@[1-5](Define all the phases in the pipeline)
@[7-11](Prepare = provision a database, provision a docker repository)
@[13-17](Build = build the app, test it and build a docker image around it. When successful, push the docker image to the docker repository.)
@[19-23](iTest = start docker containers for everything that the app is dependent on and run an test)
@[25-28](Deploy = deploy the docker container to Amazon ECS)

---

## Test Software

---

## Unit Test

```c#
        [Fact]
        public async Task Test_PutRelation_NotFound()
        {
            // Arrange - ensure that database is empty
            db.clear();
            var relationId = 123;

            // Act
            var putUrl = _controllerUrl + "/" + relationId;
            var response = await _client.PutAsJsonAsync(putUrl, relation);
            var responseContent = await response.Content.ReadAsStringAsync();

            // Assert
            response.StatusCode.GetHashCode().Should().Be(StatusCodes.Status404NotFound);
        }
```
---

## Integration Test

---

## Behavior Driven Development

---

## End-to-end Test

---

## Define Business Logic in Software

+++

```c#
    try
    {
    HOME:
        do
        {
            Play("World of Warcraft");
        }
        while (!asleep);
        Thread.Sleep(12 * 60 * 60 * 1000);
        WakeUp(coffee);
        if (you_still_give_a_shit())
            goto WORK;
        else
            goto OUT;
    WORK:
        do
        {
            if (got_something_to_do())
            {
                LookAtTheMonitor();
                Press_Some_Keys(new string[] { "Ctrl+C", "Ctrl+V" });
            }
            Browse("vbox7.com");
            Browse("topsport.bg");
            Browse("youtube.com");
            Have_a_Break();
            Have_a_Kitkat();
        }
        while (DateTime.Now.Hour < 5);

        if (DateTime.Now.Day == 1)
        {
            // at least
            GetSomeCash(3000);
        }
    OUT:
        switch (mood)
        {
            case Mood.Horny:
                ChaseChicks("hot!");
                break;
            case Mood.Dull:
                SmokeSomeStuff(new Stuff[] { "Grass", "Serious Stuff" });
                break;
            default:
                DrinkBeer(5);
                break;
        }
        goto HOME;
    }
    catch (HealthException x)
    {
        SeeTheDoctor(x);
    }
    catch (NoMoneyException)
    {
        ShitHappens();
    }
```

@[3-14](A typical day for a Developer)
@[15-28](What do yo do at work?)
@[28-35](What do yo do at work?)
@[35-49](What do yo do when you go out?)
@[50-67](Some error handling ...)

---

...

---

---?include=PITCHME_Examples.md

---?include=template/md/split-screen/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md

---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md
